function [R] = reticolo(n, p)
% n = dimensione del cluster: n x n.
% p = probabilita' di ottenere sito colorato
rng('shuffle');
R = rand(n) < p;

end

