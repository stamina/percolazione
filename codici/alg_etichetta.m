function [L] = alg_etichetta(A)
% Trova i cluster visitando tutti e quattro i vicini
% utilizzo di una lista per salvare i siti da visitare.

% R, matrice con le etichette 
R = zeros(size(A));
c = [];

% Primi vicini indice [i]: [i-1],[i+1],[i-n],[i+n]
% uso di singolo indice

% struttura L.
L.R = R;
L.c = c;
L.A = A;
I = reshape(1:numel(A),length(A),length(A));
L.NNU = I - 1;
L.NNU(1,:) = 0;
L.NND = I + 1;
L.NND(end,:) = 0;
L.NNR = I + length(A);
L.NNR(:,end) = 0;
L.NNL = I - length(A);
L.NNL(:,1) = 0;

% creo primo cluster
etichetta = 1;

% per ogni sito i
for i = 1:numel(A)
    % A(i) -> e' colorato
    % R(i) == 0 -> non assegnato a cluster o non visitato
    if A(i) && (L.R(i) == 0) 
        % chiamo la funzione esplora.m
        L = esplora(L,i,etichetta);
        etichetta = etichetta + 1;
    end
end

L.percolanti = [];
C1 = unique(L.R(find(L.R(:,1))));
for i = 1:length(C1)
    if  find(L.R(:, end) == C1(i))
        L.percolanti(end+1) = i;
    end
end

end

