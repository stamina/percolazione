function [] = simulazione(N,L,pmin,passo,pmax)
% Grafici di:   frequenza di percolazione (f_perc)
%               Redued Average Clusters Size (RACS) 
%               P media che un sito appartga a maxcluster (P1)
%               P media che sito colorato con probabilit� p sia nel cluster massimo (P2)
%               P media che sito colorato nei tentativi fatti sia nel cluster massimo (P3)
% in pratica P2 e P3, riguardano rispettivamente uno la probabilit�
% e l'altro la frequenza.
% Parametri della funzione: 
%    N         numero tentativi per probabilit�
%    L         dimensione reticoli
%    pmin      probabilit� minima
%    passo     di quanto incrementa la probabilita'
%    pmax      probabilit� masima

nP = length([pmin:passo:pmax]);
x = [pmin:passo:pmax];

Fperc = zeros(1,nP);
RACS = zeros(1,nP);
P1 = zeros(1,nP);
P2 = zeros(1,nP);
P3 = zeros(1,nP);

E_Fperc = zeros(1,nP);
E_RACS = zeros(1,nP);
E_P1 = zeros(1,nP);
E_P2 = zeros(1,nP);
E_P3 = zeros(1,nP);

i = 1;

for pr = x
    I_Fperc = zeros(1,N);
    I_RACS = zeros(1,N);
    I_P1 = zeros(1,N);
    I_P2 = zeros(1,N);
    I_P3 = zeros(1,N);
        for j = 1:N
            A = hoshen_kopelman(reticolo(L ,pr)); % crea reticolo, trova cluster 
            I_Fperc(j) = (A.percolanti(1) ~= 0); % percolazione o no?
            Smax = A.max; % dim max cluster
            dimensioni = A.cluster > 0;
            S = A.cluster(dimensioni); 
            I_RACS(j) = (sum(S.^2) - Smax^2)/sum(S); % calcolo racs
            % considero un solo elemento massimo
            I_P1(j) = Smax/(L^2);
            I_P2(j) = Smax/(pr*(L^2));
            I_P3(j) = Smax/sum(S);
        end
        
    Fperc(i) = mean(I_Fperc);
    RACS(i) = mean(I_RACS);
    P1(i) = mean(I_P1);  
    P2(i) = mean(I_P2); 
    P3(i) = mean(I_P3);
       
    E_Fperc(i) = std(I_Fperc)/sqrt(N);
    E_RACS(i) = std(I_RACS)/sqrt(N);
    E_P1(i) = std(I_P1)/sqrt(N);   
    E_P2(i) = std(I_P2)/sqrt(N);
    E_P3(i) = std(I_P3)/sqrt(N);

    i = i + 1;
end

    % FPERC
    c = figure;
    errorbar(x, Fperc, E_Fperc);
    grid on;
    xlabel('Prob. di colorazione');
    ylabel('Freq. di percolazione');
    % top = sprintf('Fperc: L = %d', L);
    % title(top);  
    name = sprintf('grafici/Fperc L=%g passo=%g N=%g',L,passo,N);
    saveas(c,strcat(name,'.fig'),'fig');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    % RACS
    c = figure;
    errorbar(x, RACS, E_RACS);
    grid on;
    xlabel('Prob. di colorazione');
    ylabel('RACS');
    % legend('RACS');
    % title(sprintf('RACS: L = %d', L));
    name = sprintf('grafici/RACS L=%g passo%g N=%g',L,passo,N);
    saveas(c,strcat(name,'.fig'),'fig');

    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    c = figure;
    hold on;
    errorbar(x, P1, E_P1,'r-');
   
    hold on;
    errorbar(x, P2, E_P2,'b-');

    hold on;
    errorbar(x, P3, E_P3,'g-');
    
    xlabel('Prob. colorazione');
    ylabel('P');
    grid on;
    
    legend('P1','P2','P3','Location','best');
    % title(sprintf('P1, P2 e P3 con L = %d', L));
    name = sprintf('grafici/P L=%g passo=%g N =%g',L,passo,N);
    saveas(c,strcat(name,'.fig'),'fig');

    
end
