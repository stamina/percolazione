function confronto_p(L,PS,camp)
% Confronto dei tempi fra HK ed 'etichetta'
% al variare della probabilita'
% avendo la dimensione del reticolo fissata (L).
%
%  L     grandezza reticolo
%  PS    accuratezza, passo (partenza a 0.01, arrivo a 0.99)
%  camp  quanti reticoli nel tempo da contare


x = 0.01 : PS : 0.99;

M_etichetta = zeros(1,length(x));
M_hk = zeros(1,length(x)); 
E_etichetta =  zeros(1,length(x));
E_HK = zeros(1,length(x));

for j = 1 : length(x)
    
    % tempi dell'algoritmo etichetta
    tic;
    for i = 1 : camp 
    alg_etichetta(reticolo(L, x(j)));
    end
    T_NHK(j) = toc;

    % tempi dell'algoritmo HK
    tic;
    for i = 1 : camp 
    hoshen_kopelman(reticolo(L, x(j)));
    end
    T_HK(j) = toc;
    
%end
    M_etichetta(j) = mean(T_NHK);
    M_hk(j) = mean(T_HK); 
    E_etichetta(j) = std(T_NHK) / sqrt(numel(T_NHK));
    E_HK(j) = std(T_HK) / sqrt(numel(T_HK));

end


a = figure;
errorbar(x, M_etichetta, E_etichetta, 'b-');
hold on
errorbar(x, M_hk, E_HK, 'g-');
grid on;
legend('t. etichetta', 't. HK','Location','best');
titolo = sprintf('Confronto Prob. etichetta e HK: L=%g PS=%g CAMP=%g',L,PS,camp);
name = sprintf('grafici/Confronto P - L=%g PS=%g CAMP=%g',L,PS,camp);
title(titolo);
xlabel('p. di colorazione');
ylabel('tempo');
saveas(a,strcat(name,'.fig'),'fig');
saveas(a,strcat(name,'.eps'),'eps');

% Disegno del grafico
end