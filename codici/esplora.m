function[L] = esplora(L,i,etichetta)
% esplora un sito e gestisce le visite sui vicini.

lista = [i];
base = 1; % il puntatore a dx di lista, indice

% i-esimo sito appartiene al cluster etichetta
L.R(i) = etichetta; 

% length (lista) corrisponde al puntatore sx, che indica la fine.
while base <= length(lista)
    
    %%%%%%%%%%%%%%%%%
    %(1) L.NNU(pila(base)):= elem. UP della base della lista
    %(2) L.A(L.NNU(pila(base))):= guardo se (1) e' colorato
    %(3) (L.R(L.NNU(pila(base))) == 0) := guardo se (1) non e' visitato
    % quindi controllato esista UP, che sia colorato e non visitato
    if L.NNU(lista(base)) && L.A(L.NNU(lista(base))) ...
        && (L.R(L.NNU(lista(base))) == 0)
         
        % aumento lista col nuovo;
        % lo inserisco a fine
        % cosi' controllo vicini, poi assegno etichetta.
        lista(end+1) = L.NNU(lista(base));
        L.R(L.NNU(lista(base))) = etichetta;
    end
    
    %%%%%%%%%%%%%%%%%
    % idem NNU, ma NNR
    if L.NNR(lista(base)) && L.A(L.NNR(lista(base))) ...
        && (L.R(L.NNR(lista(base))) == 0)

        lista(end+1) = L.NNR(lista(base));
        L.R(L.NNR(lista(base))) = etichetta;
    end
    %%%%%%%%%%%%%%%%%
    % idem NNU, ma NND
    if L.NND(lista(base)) && L.A(L.NND(lista(base))) ...
        && (L.R(L.NND(lista(base))) == 0)

        lista(end+1) = L.NND(lista(base));
        L.R(L.NND(lista(base))) = etichetta;
    end
    %%%%%%%%%%%%%%%%
    % idem NNU, ma NNL
    if L.NNL(lista(base)) && L.A(L.NNL(lista(base))) ...
        && (L.R(L.NNL(lista(base))) == 0)

        lista(end+1) = L.NNL(lista(base));
        L.R(L.NNL(lista(base))) = etichetta;
    end   
    % incremento della base al successivo della lista
    base = base +1;
end; % fine while
L.c(etichetta) = length(lista);
end

