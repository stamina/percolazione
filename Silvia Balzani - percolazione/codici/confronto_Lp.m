function confronto_Lp(L,p, camp)
% Confronto dei tempi fra HK ed 'etichetta'
% al variare della probabilita' e di L.
% Su asse x gli elementi di L passati,
% Su asse y gli elemtni di p passati,
% Sy asse z i tempi calcolati.
%
% dim dimensione del reticolo
% camp il numero di cluster find da eseguire ne tic toc

% L       asse x
% p       asse y
% tempo   asse z


dimX = length(L);
dimY = length(p);

T_NHK = zeros(dimY,dimX);
T_HK = zeros(dimY,dimX);

[xx,yy] = meshgrid(L,p) 
% xx = [];
% yy = [];
% for i = 1: dimY*dimX
%     xx(i) = L(mod(i,dimX) +1);
% end
% xx
% for j = 1: dimY
%     for i = 1 : dimX 
%         yy = [yy p(j)];
%     end
% end
% k = 1;
% length(xx)
% yy
% length(yy)
% xx = reshape(xx,dimX,dimY);
% yy = reshape(yy,dimX,dimY);
xx
yy




for i = 1:dimX*dimY

        % tempi dell'algoritmo etichetta
        tic;
        for f = 1 : camp 
        alg_etichetta(reticolo(xx(i), yy(i)));
        end
        T_NHK(i) = toc;
        T_NHK

        % tempi dell'algoritmo HK
        tic;
        for f = 1 : camp 
        hoshen_kopelman(reticolo(xx(i), yy(i)));
        end
        T_HK(i) = toc;
        T_HK

end

        %end
            %E_etichetta(k) = std(T_NHK(i,j)) / sqrt(numel(T_NHK));
            %E_HK(k) = std(T_HK) / sqrt(numel(T_HK));
%             k = k + 1;
% 
%     end
% end


a = figure;
surf(xx, yy, T_NHK);
title('Confronto al variare di L e p');
xlabel('dimensione reticolo (L)');
ylabel('prob.');
zlabel('tempo');
grid on


b = figure;
surf(xx, yy,T_HK);
title('Confronto al variare di L e p');
xlabel('dimensione reticolo (L)');
ylabel('prob.');
zlabel('tempo');
grid on;
%legend('t. etichetta', 't. HK','Location','best');
% title('Confronto al variare di L e p');
% xlabel('dimensione reticolo (L)');
% ylabel('prob.');
% zlabel('tempo');
saveas(a,strcat('grafici/Confronto PL NHK','.fig'),'fig');
saveas(b,strcat('grafici/Confronto PL HK','.fig'),'fig');

% Disegno del grafico
end