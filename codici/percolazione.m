function [frequenza] = percolazione(n, N, p)
% 
% ritorna la frequenza
% n ordine matrice
% N tentativi
% p probabilitÓ (pu˛ essere matrice contenente diverse possibilitÓ
    % -> ciclerÓ su "pr").

for pr = 1: length(p) % esegue il tutto sulle diverse probabilitÓ
    successi = 0;
    for i = 1:N % tutti e N i tentativi per quella probabilitÓ
        % creazione del reticolo, 1 se colorato.
        % p(pr) probabilitÓ che sia colorato.
        A = reticolo(n,p(pr));
        % trova cluster all'interno del reticolo A
        % due possibili algoritmi: "etichetta" e Hoshen-Kepelman.
        L = trova_cluster(A);
        if isempty(L.percolanti) == 0
            successi = successi + 1;
        end
    end
    frequenza(pr) = successi / N;
end

end

