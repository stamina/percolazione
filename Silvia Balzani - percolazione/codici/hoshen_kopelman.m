function [L] = hoshen_kopelman(A)
% Algoritmo per trovare i cluster nel reticolo A
% Per creare reticolo utilizzare reticolo(n,p).
% 
% L = hoshen_kepelman(A)
% L.A           reticolo
% L.et          reticolo con stampate etichette cluster
% L.n           dimensione reticolo
% SX            vicini sinistri
% TX            vicini superiori
% L.cluster     i cluster
%                   (se negativo, il modulo = vera etichetta,
%                    se positivo, la dimensione del cluster)
% L.percolanti  cluster percolanti
% L.max         dimensione del cluster massimo

L.et = zeros(size(A)); % etichette assegnate
L.A = A; % reticolo (0 e 1 di colorazione)
L.n = length(A); % dimensione reticolo
MAXet = 1;


% Primi vicini successivi
I = reshape(1:numel(A),length(A),length(A));

TX = I - 1; % sopra
SX = I - L.n; % sinistra
TX(1,:) = 0;
SX(:,1) = 0;
L.cluster = zeros(1,floor((L.n^2)/2));


for i= 1:((L.n)^2)
    if L.A(i) == 1 % se i colorato
        
        % assegno sopra
        if TX(i) == 0 || L.A(TX(i)) == 0
            sopra = 0;
        elseif L.cluster(L.et(TX(i))) < 0
            sopra = abs(L.cluster(L.et(TX(i))));
        else
            sopra = L.et(TX(i));
        end    
        
        % assegno sinistra
        if SX(i) == 0 || L.A(SX(i)) == 0
            sinistra = 0;
        elseif L.cluster(L.et(SX(i))) < 0
            sinistra = abs(L.cluster(L.et(SX(i))));
        else
            sinistra = L.et(SX(i));
        end
        
                   
        % se non colorati o non etichettati
        if (sopra == 0 && sinistra == 0)
            L.et(i) = MAXet; % assegno massima et. non usata
            L.cluster(MAXet) = 1;
            MAXet = MAXet + 1; % increm di etichetta massima
            
            
        %   etichetta a sinistra 
        elseif  sinistra ~= 0 && sopra == 0
            % etichetta sx diversa da zero
                    L.cluster(sinistra) = L.cluster(sinistra) + 1;
                    L.et(i) = sinistra;
            
        %  etichetta sopra
        elseif sinistra == 0 && sopra~=0
                % etichetta sopra diversa da zero
                    L.cluster(sopra) = L.cluster(sopra) + 1;
                    L.et(i) = sopra;

            
       % etichetta sinistra e sopra uguali (non uguali a zero)
       elseif sinistra == sopra
                    L.et(i) = sinistra;
                    L.cluster(sinistra) = L.cluster(sinistra)+1;

  
       
        else
            % etichette sinistra e sopra diverse
            et1 = sopra;
            et2 = sinistra;           

            minimo = min(sopra,sinistra);
            massimo = max(sopra,sinistra);
            
            if (et1 ~= et2)
                somma = 1 + (L.cluster(et1)) + (L.cluster(et2));
                %  et. piu' piccola va in piu' grande
                L.cluster(minimo) = - massimo;  
            else
                somma = 1 + (L.cluster(et1));
            end
            L.cluster(massimo) = somma;
            L.et(i) = massimo;
            altri = (L.cluster(1:massimo) == (-minimo));
            L.cluster(altri) = - massimo;
        end
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% intersezione prima e ultima colonna
% reperisco i cluster della prima colonna

indici = L.A(:,1) ~= 0;
etichette = L.et(indici);
primacolonna = L.cluster(etichette) < 0;
clusternonalias = L.cluster(etichette) > 0;
insieme1 = [abs(L.cluster(etichette(primacolonna))), clusternonalias];	

% intersezione cluster prima e ultima riga
L.percolanti = intersect(insieme1,L.et(:,L.n));


if isempty(L.percolanti) % se non ho nessun percolante
    L.percolanti = 0; % ritorna 0
    
elseif L.percolanti(1) == 0
        L.percolanti(1) = []; 
        if isempty(L.percolanti) % se non ho nessun percolante
                L.percolanti = 0;
        end
    
end


% cancella i cluster che non devono comparire
L.cluster(MAXet:length(L.cluster)) = [];

% setta la dimensione massima dei cluster raggiunta
L.max = max(L.cluster);
end

